// `include "./paralelo_a_serial/PS_COM_cond.v"

module serializacion_cond(   input clk_8f,
                        input reset_L,
                        input [7:0] data_inPS0, // SALIDA DEL FIFO ANTERIOR
                        input valid_inPS0, // POP DEL FIFO ANTERIOR, SALE DEL ARBITRO
                        input [7:0] data_inPS1, // SALIDA DEL FIFO ANTERIOR
                        input valid_inPS1, // POP DEL FIFO ANTERIOR, SALE DEL ARBITRO
                        output data_out_serial0_cond,
                        output data_out_serial1_cond);
        
    PS_COM_cond PS0(.clk_8f(clk_8f),
                    .reset_L(reset_L),
                    .data_in(data_inPS0),
                    .valid_in(valid_inPS0),
                    .data_out(data_out_serial0_cond));
    
    PS_COM_cond PS1(.clk_8f(clk_8f),
                    .reset_L(reset_L),
                    .data_in(data_inPS1),
                    .valid_in(valid_inPS1),
                    .data_out(data_out_serial1_cond));

endmodule