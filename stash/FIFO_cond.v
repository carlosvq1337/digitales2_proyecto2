// `include "./memoria/memoria_cond.v"

module fifo_cond #(parameter BW = 10,
					parameter [3:0] LEN = 8)(
	input i_clk, i_reset,
	input [BW-1:0]	i_data,
	input i_rd,
	input i_wr, 
    output [BW-1:0] o_data_cond,
	output reg o_empty_cond,
	output reg o_full_cond,
	output reg o_almost_full_cond,
	output reg o_almost_empty_cond);

	reg [2:0] rd_ptr;
	reg [2:0] wr_ptr;
	reg empty;
	reg full;
	reg i_rd0;

	memoria_cond #(.BW(BW), .LEN(LEN)) mem  (
        .o_data_cond	(o_data_cond),
        .datain   	(i_data),
        .addr_in   	(wr_ptr),
        .addr_out  	(rd_ptr),
		.read		(i_rd0),
		.write		(i_wr),
		.reset     	(i_reset),
        .clk       	(i_clk));
	
	always @(posedge i_clk) begin
		if (~i_reset) begin
			rd_ptr<=0;
			wr_ptr<=0;
			empty<=1;
			full<=0;
		end else begin
			if(i_rd& !o_empty_cond) rd_ptr <= rd_ptr +1;
			if(i_wr& !o_full_cond) wr_ptr <= wr_ptr +1;
			if(o_almost_full_cond|o_full_cond) begin
				full <= 1;
			end else begin
				full <= 0;
			end
			if(o_almost_empty_cond|o_empty_cond) begin
				empty <= 1;
			end else begin
				empty <= 0;
			end
		end
	end

	always @(*) begin
		o_almost_full_cond = 0;
		o_almost_empty_cond = 0;
		o_full_cond = 0;
		o_empty_cond = 0;
		i_rd0=0;
		if (rd_ptr > wr_ptr) begin
			if (rd_ptr-wr_ptr == 1) o_almost_full_cond = 1;
			if (rd_ptr-wr_ptr == LEN-1) o_almost_empty_cond = 1;
		end else begin
			if (wr_ptr-rd_ptr == LEN-1) o_almost_full_cond = 1;
			if (wr_ptr-rd_ptr == 1) o_almost_empty_cond = 1;
		end		
		if (wr_ptr == rd_ptr & empty) o_empty_cond = 1;
		if (wr_ptr == rd_ptr & full) o_full_cond = 1;
		if (i_rd&!o_empty_cond) i_rd0=1;
	end
endmodule
/*
	always @ (posedge i_clk) begin
		if (~i_reset)
			sr_i_rd_i_wr_empty <= 1'b0;
		else if(i_rd == 1'b1 && empty == 1'b1 && i_wr == 1'b1)
			sr_i_rd_i_wr_empty <= 1'b1;
		else
			sr_i_rd_i_wr_empty <= 1'b0;
	end

	always @ (posedge i_clk) begin
		if (~i_reset)
			o_data_cond <= 8'h0;
		else if(i_wr == 1'b1 && empty == 1'b0)
			o_data_cond <= fifo_mem[tail];
	end

	always @ (posedge i_clk) begin
		if (~i_reset)
			fifo_mem[head] <= 8'h0;
		else if(i_rd == 1'b1 && full == 1'b0)
			fifo_mem[head] <= i_data;
	end
	
	always @ (posedge i_clk) begin
		if (~i_reset)
			wr_ptr <= 3'b000;
		else begin
			if(i_wr == 1'b1)
				wr_ptr <= wr_ptr+1'b1;
		end
	end

	always @ (posedge i_clk) begin
		if (~i_reset)
			rd_ptr <= 3'b000;
		else begin
			if(i_rd == 1'b1)
				rd_ptr <= rd_ptr +1'b1;
		end
	end

	always @ (posedge i_clk) begin
		if (~i_reset)
			head <= 3'b000;
		else begin
			if(i_wr == 1'b1 && full == 1'b0)
				head <= head +1'b1;
		end
	end

	always @ (posedge i_clk) begin
		if (~i_reset)
			tail <= 3'b000;
		else begin
			if(i_rd == 1'b1 && empty == 1'b0)
				tail <= tail +1'b1;
		end
	end

	always @ (posedge i_clk) begin
		if (~i_reset)
			count <= 3'b000;
		else begin
			case ({i_rd, i_wr})
			2'b00: count <= count;
			2'b01:	if(count != 3'b111)
						count <= count +1;
			2'b10:	if(count != 3'b000)
						count <= count +1;
			2'b11:	if(sr_i_rd_i_wr_empty == 1'b1)
						count <= count +1;
					else
						count <= count;
			default: count <= count;
			endcase
		end
	end

	always @ (*) begin
		if (rd_ptr == wr_ptr )
			empty <= 1'b1;
		else 
			empty <= 1'b0;
	end

	always @ (*) begin
		if (rd_ptr + wr_ptr == 3'b111)
			full <= 1'b1;
		else 
			full <= 1'b0;
	end
endmodule

/*module FIFO_c( 
    input	i_clk, i_reset,
	input [7:0]	i_data,
    output	empty, full,
    output reg [7:0] o_data_cond);
	
    reg [2:0]  Count = 0; 
    reg [7:0] FIFO [0:7];
    reg [2:0]  i_rdCounter = 0, i_wrCounter = 0; 

    assign empty = (Count==0)? 1'b1:1'b0; 

    assign full = (Count==8)? 1'b1:1'b0; 

    always @ (posedge i_clk) begin 
		if (i_reset) begin 
			i_rdCounter = 0; 
			i_wrCounter = 0; 
		end 
		else if (Count!=0) begin 
			o_data_cond  = FIFO[i_rdCounter]; 
			i_rdCounter = i_rdCounter+1; 
		end 
		else if (Count<8) begin
			FIFO[i_wrCounter]  = i_data; 
			i_wrCounter  = i_wrCounter+1; 
		end 
		else;  

	    if (i_wrCounter==8) 
			i_wrCounter=0; 
	    else if (i_rdCounter==8) 
		    i_rdCounter=0; 
	    else;

    	if (i_rdCounter > i_wrCounter) begin 
		    Count=i_rdCounter-i_wrCounter; 
	    end 

    	else if (i_wrCounter > i_rdCounter) 
		    Count=i_wrCounter-i_rdCounter; 
	    else;
	end 
endmodule*/
