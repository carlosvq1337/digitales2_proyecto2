`timescale 1ns / 100ps
`include "probador_D1.v"
`include "dispositivo1_cond.v"
`include "clasificacion_cond.v"
`include "enrutamiento_cond.v"
`include "arbitro_cond.v"
`include "serializacion_cond.v"
`include "dmux12_10b_cond.v"
`include "mux21_10b_cond.v"
`include "FIFO_cond.v"
`include "memoria_cond.v"
`include "PS_COM_cond.v"
`include "cmos_cells.v"
`include "dispositivo1_estr.v"

module testbench;

//PARAMS
parameter BW=10;

//INPUTS
wire clk, clk_8f, reset_L, clase, dest, push;
wire [7:0] data_in;

//OUTPUTS
wire data_out_serial0_cond, data_out_serial1_cond;
wire data_out_serial0_estr, data_out_serial1_estr; 


dispositivo1_cond D1_cond(
    //inputs
    .clk        (clk),
    .clk_8f     (clk_8f),
    .reset_L    (reset_L),
    .clase      (clase),
    .dest       (dest),
    .push       (push),
    .data_in    (data_in),

    //outputs
    .data_out_serial0_cond   (data_out_serial0_cond),
    .data_out_serial1_cond   (data_out_serial1_cond)

);

dispositivo1_estr D1_estr(
    //inputs
    .clk        (clk),
    .clk_8f     (clk_8f),
    .reset_L    (reset_L),
    .clase      (clase),
    .dest       (dest),
    .push       (push),
    .data_in    (data_in),

    //outputs
    .data_out_serial0_estr     (data_out_serial0_estr),
    .data_out_serial1_estr     (data_out_serial1_estr)

);

probador p_D1 (
    //outputs
    .data_in                    (data_in[7:0]),
    .clk                        (clk),
    .clk_8f                     (clk_8f),
    .push                       (push),
    .reset_L                    (reset_L),
    .clase                      (clase),
    .dest                       (dest),
    
    //inputs
    .data_out_serial0_cond           (data_out_serial0_cond),
    .data_out_serial1_cond           (data_out_serial1_cond),
    .data_out_serial0_estr           (data_out_serial0_estr),
    .data_out_serial1_estr           (data_out_serial1_estr)
);

endmodule