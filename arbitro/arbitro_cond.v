module arbitro_cond (
	input [1:0]	empty_clasif,  //flags empty de las FIFOS de clasificación
    input [1:0] empty_enrut, //flags empty para las FIFOs de enrutamiento
    input [1:0] full,   //flags full de las FIFOS de enrutamiento
    input [1:0] almost_full_D2,
    input dest,         //bits de destino (será necesario separarlos de o_data en el banco de pruebas
	output reg push_cond, //salida push para las FIFOS de enrutamiento 
    output reg [1:0] pop_clasif_cond,  //salida pop para las FIFOS de clasificación
    output reg [1:0] pop_enrut_cond,    //salida pop para las FIFOs de enrutamiento
    // output reg selector_demux_cond, //control del demux de enrutamiento
    output reg selector_mux_cond //control del mux
    ); 


    always @(*) begin
        casez ({empty_clasif, empty_enrut})
        6'b?000: begin
            selector_mux_cond = 0;
            
            // selector_demux_cond = dest[0];
            if (full[dest] == 0) begin
                pop_clasif_cond = 2'b01;
                push_cond = 1;
            end else begin
                push_cond =0;
                pop_clasif_cond = 2'b00;
            end
            pop_enrut_cond = (~almost_full_D2) & (~empty_enrut);
        end

        6'b?011: begin
            selector_mux_cond = 0;
            
            // selector_demux_cond = dest[0];
            if (full[dest] == 0) begin
                push_cond = 1;
                pop_clasif_cond = 2'b01;
            end else begin
                push_cond =0;
                pop_clasif_cond = 2'b00;
            end
            pop_enrut_cond = (~almost_full_D2) & (~empty_enrut);
        end

        6'b?001: begin
            selector_mux_cond = 0;
            
            // selector_demux_cond = dest[0];
            if (full[dest] == 0) begin
                pop_clasif_cond = 2'b01;
                push_cond = 1;
            end else begin
                push_cond =0;
                pop_clasif_cond = 2'b00;
            end
            pop_enrut_cond = (~almost_full_D2) & (~empty_enrut);
        end

        6'b?010: begin
            selector_mux_cond = 0;
            
            // selector_demux_cond = dest[0];
            if (full[dest] == 0) begin
                pop_clasif_cond = 2'b01;
                push_cond = 1;
            end else begin
                push_cond =0;
                pop_clasif_cond = 2'b00;
            end
            pop_enrut_cond = (~almost_full_D2) & (~empty_enrut);
        end


        //CASOS DONDE FIFO0 de clasif, está vacía

        6'b0100: begin
            selector_mux_cond = 1;
            
            // selector_demux_cond = dest[0];
            if (full[dest] == 0) begin
                push_cond = 1;
                pop_clasif_cond = 2'b10;
            end else begin
                push_cond =0;
                pop_clasif_cond = 2'b00;
            end
            pop_enrut_cond = (~almost_full_D2) & (~empty_enrut);
        end

        6'b0111: begin
            selector_mux_cond = 1;
            
            // selector_demux_cond = dest[0];
            if (full[dest] == 0) begin
                push_cond = 1;
                pop_clasif_cond = 2'b10;
            end else begin
                push_cond =0;
                pop_clasif_cond = 2'b00;
            end
            pop_enrut_cond = (~almost_full_D2) & (~empty_enrut);
        end

        6'b0101: begin
            selector_mux_cond = 1;
            
            // selector_demux_cond = dest[0];
            if (full[dest] == 0) begin
                push_cond = 1;
                pop_clasif_cond = 2'b10;
            end else begin
                push_cond =0;
                pop_clasif_cond = 2'b00;
            end
            pop_enrut_cond = (~almost_full_D2) & (~empty_enrut);
        end

        6'b0110: begin
            selector_mux_cond = 1;
            
            // selector_demux_cond = dest[0];
            if (full[dest] == 0) begin
                pop_clasif_cond = 2'b10;
                push_cond = 1;
            end else begin
                push_cond =0;
                pop_clasif_cond = 2'b00;
            end
            pop_enrut_cond = (~almost_full_D2) & (~empty_enrut);
        end

        //CASOS DONDE AMBAS FIFOs DE CLASIF. ESTÁN VACIAS


        6'b1100: begin
            selector_mux_cond = 1;
            pop_clasif_cond = 2'b00;
            push_cond = 0;
            // selector_demux_cond = dest[0];
            pop_enrut_cond = (~almost_full_D2) & (~empty_enrut);
        end

        6'b1111: begin
            selector_mux_cond = 1;
            pop_clasif_cond = 2'b00;
            push_cond = 0;
            // selector_demux_cond = dest[0];
            pop_enrut_cond = (~almost_full_D2) & (~empty_enrut);
        end

        6'b1101: begin
            selector_mux_cond = 1;
            pop_clasif_cond = 2'b00;
            push_cond = 0;
            // selector_demux_cond = dest[0];
            pop_enrut_cond = (~almost_full_D2) & (~empty_enrut);
        end

        6'b1110: begin
            selector_mux_cond = 1;
            pop_clasif_cond = 2'b00;
            push_cond = 0;
            // selector_demux_cond = dest[0];
            pop_enrut_cond = (~almost_full_D2) & (~empty_enrut);
        end
        default: begin
            pop_clasif_cond = 2'b00;
            pop_enrut_cond = 2'b00;
            push_cond = 0;
            selector_mux_cond = 0;
        end

       
        endcase
    end
    
endmodule