module probador #(parameter BW = 10)(
	output reg reset_L,
    output reg clk,
    output reg [7:0] data_in,
    output reg clase,
    output reg push,
    output reg dest,
    output reg [1:0] i_rd,
    output reg selector_mux_cond,
    output reg [1:0] pop_clasif_cond,
    output reg push_cond,
    output reg [1:0] pop_enrut_cond,
    input [(BW-1):0] o_data_condP0,
    input o_error_condP0,
    input o_full_condP0,
    input o_empty_condP0,
    input o_almost_full_condP0,
    input o_almost_empty_condP0,
    input [(BW-1):0] o_data_condP1,
    input o_error_condP1,
    input o_full_condP1,
    input o_empty_condP1,
    input o_almost_full_condP1,
    input o_almost_empty_condP1);

initial begin
    $dumpfile("waves.vcd");
    $dumpvars;
    push<=0;
    clase<=0;
    dest<=0;
    i_rd<=0;
    data_in<=8'h00;
    reset_L<=0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    reset_L <= 1;
    push <= 1;
    data_in<=8'h05;
    @(posedge clk);
    data_in<=8'hFF;
    @(posedge clk);
    data_in<=8'hBC;
    @(posedge clk);
    data_in<=8'h01;
    @(posedge clk);
    data_in<=8'h02;
    clase <= 1;
    @(posedge clk);
    data_in<=8'h03;
    @(posedge clk);
    data_in<=8'h04;
    @(posedge clk);
    data_in<=8'h05;
    @(posedge clk);
    data_in<=8'h06;
    @(posedge clk);
    data_in<=8'h07;
    @(posedge clk);
    repeat (5) begin
    @(posedge clk);
    end

    repeat (5) begin
    @(posedge clk);
    end

    repeat (8) begin
    @(posedge clk);
    end


    $finish;
end


initial clk <= 0; 
always #8 clk <= ~clk;

endmodule