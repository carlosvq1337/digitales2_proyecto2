`timescale 1ns / 100ps
`include "probador_completo.v"
`include "dispositivo1_cond.v"
`include "dispositivo2_cond.v"
`include "clasificacion_cond.v"
`include "enrutamiento_cond.v"
`include "arbitro_cond.v"
`include "serializacion_cond.v"
`include "dmux12_10b_cond.v"
`include "mux21_10b_cond.v"
`include "FIFO_cond.v"
`include "memoria_cond.v"
`include "PS_COM_cond.v"
`include "SP_COM_cond.v"
`include "cmos_cells.v"
`include "dispositivo1_estr.v"
`include "dispositivo2_estr.v"

module testbench;

//PARAMS
parameter BW=10;

//INPUTS
wire clk, clk_8f, reset_L, clase, dest, push;
wire [7:0] data_in;
wire [1:0] pop;

//INTERNAL
wire [1:0] almost_full_D2_cond, almost_full_D2_estr;

//OUTPUTS
wire data_out_serial0_cond, data_out_serial1_cond;
wire data_out_serial0_estr, data_out_serial1_estr;
wire [7:0] data_out0_cond, data_out1_cond;
wire [7:0] data_out0_estr, data_out1_estr;


dispositivo1_cond D1_cond(
    //inputs
    .clk        (clk),
    .clk_8f     (clk_8f),
    .reset_L    (reset_L),
    .clase      (clase),
    .dest       (dest),
    .push       (push),
    .data_in    (data_in),
    .almost_full_D2 (almost_full_D2_cond),

    //outputs
    .data_out_serial0_cond   (data_out_serial0_cond),
    .data_out_serial1_cond   (data_out_serial1_cond)

);

dispositivo1_estr D1_estr(
    //inputs
    .clk        (clk),
    .clk_8f     (clk_8f),
    .reset_L    (reset_L),
    .clase      (clase),
    .dest       (dest),
    .push       (push),
    .data_in    (data_in),
    .almost_full_D2 (almost_full_D2_estr),

    //outputs
    .data_out_serial0_estr     (data_out_serial0_estr),
    .data_out_serial1_estr     (data_out_serial1_estr)

);


dispositivo2_cond D2_cond(
    //inputs
    .clk             (clk),
    .clk_8f          (clk_8f),
    .reset_L         (reset_L),
    .data_inSP0      (data_out_serial0_cond),
    .data_inSP1      (data_out_serial1_cond),
    .pop             (pop),

    //outputs
    .data_out0_cond   (data_out0_cond[7:0]),
    .data_out1_cond   (data_out1_cond[7:0]),
    .almost_full_D2_cond (almost_full_D2_cond[1:0])

);

dispositivo2_estr D2_estr(
    //inputs
    .clk            (clk),
    .clk_8f         (clk_8f),
    .reset_L        (reset_L),
    .data_inSP0     (data_out_serial0_estr),
    .data_inSP1     (data_out_serial1_estr),
    .pop            (pop),

    //outputs
    .data_out0_estr     (data_out0_estr[7:0]),
    .data_out1_estr     (data_out1_estr[7:0]),
    .almost_full_D2_estr (almost_full_D2_estr[1:0])

);

probador p_completo (
    //outputs
    .data_in                    (data_in[7:0]),
    .clk                        (clk),
    .clk_8f                     (clk_8f),
    .push                       (push),
    .reset_L                    (reset_L),
    .clase                      (clase),
    .dest                       (dest),
    .pop                        (pop[1:0]),
    
    //inputs
    .data_out_serial0_cond           (data_out_serial0_cond),
    .data_out_serial1_cond           (data_out_serial1_cond),
    .data_out_serial0_estr           (data_out_serial0_estr),
    .data_out_serial1_estr           (data_out_serial1_estr),
    .data_out0_cond           (data_out0_cond),
    .data_out1_cond           (data_out1_cond),
    .data_out0_estr           (data_out0_estr),
    .data_out1_estr           (data_out1_estr)
);

endmodule