module probador #(parameter BW = 10)(
	output reg reset_L,
    output reg clk,
    output reg clk_8f,
    output reg [7:0] data_in,
    output reg clase,
    output reg push,
    output reg dest,
    input data_out_serial0_cond,
    input data_out_serial1_cond,
    input data_out_serial0_estr,
    input data_out_serial1_estr);

reg clk_4f, clk_2f;

initial begin
    $dumpfile("waves.vcd");
    $dumpvars;
    push<=0;
    clase<=0;
    dest<=0;
    data_in<=0;
    reset_L<=0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    reset_L <= 1;
    push <= 1;
    data_in<=0;
    @(posedge clk);
    data_in<=1;
    @(posedge clk);
    data_in<=2;
    @(posedge clk);
    data_in<=3;
    @(posedge clk);
    clase <= 1;
    dest <= 0;
    data_in<=4;
    @(posedge clk);
    data_in<=5;
    @(posedge clk);
    data_in<=6;
    @(posedge clk);
    data_in<=7;
    @(posedge clk);
    clase <= 0;
    dest <= 1;
    data_in<=8;
    @(posedge clk);
    data_in<=9;
    @(posedge clk);
    data_in<='hA;
    @(posedge clk);
    data_in<='hB;
    @(posedge clk);
    clase <= 1;
    dest <= 1;
    data_in<='hC;
    @(posedge clk);
    data_in<='hD;
    @(posedge clk);
    data_in<='hE;
    @(posedge clk);
    data_in<='hF;
    @(posedge clk);
    push <= 0;
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    @(posedge clk);
    $finish;
end

always @ (posedge clk_8f) begin
    clk_4f <= ~clk_4f;
end

always @ (posedge clk_4f) begin
    clk_2f <= ~clk_2f;
end

always @ (posedge clk_2f) begin
    clk <= ~clk;
end

initial clk <= 0; 
initial clk_2f <= 0; 
initial clk_4f <= 0; 
initial clk_8f <= 0; 
always #8 clk_8f <= ~clk_8f;


endmodule