#---------------------------------------------------------------------
# Input dirs, names, files
#---------------------------------------------------------------------

# Includes
IDIR= demux mux FIFOs memoria enrutamiento clasificacion paralelo_a_serial serial_a_paralelo dispositivo1 dispositivo2 serializacion arbitro cells
INC=$(foreach d, $(IDIR), -I $d)

# Objetos
ODIR = obj
MKDIR_P = mkdir -p
_OBJ = TB_completo.o

OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

# VCD
VCD = waves.vcd

# Top module
TOP = testbench


#---------------------------------------------------------------------
# rules
#---------------------------------------------------------------------
.PHONY: all gtkwave yosys clean directories

all: directories yosys iverilog gtkwave

directories: ${ODIR}

iverilog: $(VCD)

gtkwave:
	gtkwave $(VCD)


yosys:
	yosys scripts/FIFO_script.ys
	sed -i 's/_cond/_estr/g' FIFOs/FIFO_estr.v

	yosys scripts/synthMux.ys
	sed -i 's/_cond/_estr/g' mux/mux21_10b_estr.v

	yosys scripts/synthDmux.ys
	sed -i 's/_cond/_estr/g' demux/dmux12_10b_estr.v

	yosys scripts/clasificacion_script.ys
	sed -i 's/_cond/_estr/g' clasificacion/clasificacion_estr.v

	yosys scripts/enrutamiento_script.ys
	sed -i 's/_cond/_estr/g' enrutamiento/enrutamiento_estr.v

	yosys scripts/arbitro_script.ys
	sed -i 's/_cond/_estr/g' arbitro/arbitro_estr.v

	yosys scripts/serializacion_script.ys
	sed -i 's/_cond/_estr/g' serializacion/serializacion_estr.v

	yosys scripts/dispositivo1_script.ys
	sed -i 's/_cond/_estr/g' dispositivo1/dispositivo1_estr.v

	yosys scripts/dispositivo2_script.ys
	sed -i 's/_cond/_estr/g' dispositivo2/dispositivo2_estr.v
	
	
clean:
	rm -f $(ODIR)/*.o
	rm -f *.vcd

$(ODIR)/%.o: %.v
	iverilog -o $@ -Ttyp $(INC) -s $(TOP) $<

$(VCD): $(OBJ)
	vvp $^

${ODIR}:
		${MKDIR_P} ${ODIR}
