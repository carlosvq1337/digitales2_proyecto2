module SP_IDL_cond (
                    input       clk_32f,
                    input       reset_L,
                    input       data_in,
                    output reg  IDLE_out
                    );

    integer BC_counter;
    reg hubo_BC;
    integer p_counter;
    reg [7:0] buffer;

    always @ (posedge clk_32f) begin
        if (reset_L == 0) begin
            buffer <= 0;
            p_counter <= 0;
            BC_counter <= 0;
            hubo_BC <= 0;
            IDLE_out <= 0;
        end else begin
            buffer <= {buffer[6:0], data_in};
            if (hubo_BC == 1) begin
                p_counter <= p_counter + 1;
                if (buffer == 8'hBC) begin
                    p_counter <= 0;
                    BC_counter <= BC_counter + 1;
                end else if (buffer == 8'h7C & BC_counter >= 4)begin
                    IDLE_out <= 1;
                end else if (p_counter == 7) begin
                    p_counter <= 0;
                end
            end else begin
                if (buffer == 8'hBC) begin
                BC_counter <= BC_counter + 1;
                hubo_BC <= 1;
                end
            end
        end
    end

endmodule