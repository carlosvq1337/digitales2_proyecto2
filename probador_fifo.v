module probador (
	output reg i_reset,
    output reg i_clk,
    output reg i_wr,
    output reg i_rd,
    output reg [(BW-1):0] i_data,
    input [(BW-1):0] o_data_cond,
    input [(BW-1):0] o_data_estr,
    input o_error_cond,
    input o_error_estr,
    input o_full_cond,
    input o_empty_cond,
    input o_full_estr,
    input o_empty_estr,
    input o_almost_full_cond,
    input o_almost_empty_cond,
    input o_almost_full_estr,
    input o_almost_empty_estr);
    parameter	BW=8;	// Byte/data width

initial begin
    $dumpfile("waves.vcd");
    $dumpvars;
    i_wr<=0;
    i_rd<=0;
    i_data<=8'h00;
    i_reset<=0;
    @(posedge i_clk);
    @(posedge i_clk);
    i_reset<=1;
    @(posedge i_clk);
    i_data<=8'h05;
    i_wr<=1;
    @(posedge i_clk);
    i_data<=8'hFF;
    i_rd<=0;
    @(posedge i_clk);
    i_data<=8'hBC;
    @(posedge i_clk);
    i_data<=8'h01;
    @(posedge i_clk);
    i_data<=8'h02;
    @(posedge i_clk);
    i_data<=8'h03;
    @(posedge i_clk);
    i_data<=8'h04;
    @(posedge i_clk);
    i_data<=8'h05;
    @(posedge i_clk);
    i_data<=8'h06;
    @(posedge i_clk);
    i_data<=8'h07;
    @(posedge i_clk);
    i_wr<=0;
    i_rd<=1;
    repeat (5) begin
    @(posedge i_clk);
    end
    i_rd <=0;
    repeat (5) begin
    @(posedge i_clk);
    end
    i_rd <=1;
    repeat (8) begin
    @(posedge i_clk);
    end


    $finish;
end


initial i_clk <= 0; 
always #8 i_clk <= ~i_clk;

endmodule