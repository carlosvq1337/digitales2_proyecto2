module dispositivo2_cond #( parameter BW=8,
                            parameter LEN=4)
                          ( input clk,
                            input clk_8f,
                            input reset_L,
                            input data_inSP0,
                            input data_inSP1,
                            input  [1:0] pop,
                            output [1:0] almost_full_D2_cond,
                            output [7:0] data_out0_cond,
                            output [7:0] data_out1_cond);
    
    wire [7:0] data_out_SP0_cond, data_out_SP1_cond;
    wire valid_outSP0_cond, valid_outSP1_cond;
    wire o_full_cond0, o_full_cond1, o_empty_cond0, o_empty_cond1;
    wire o_almost_full_cond0, o_almost_full_cond1, o_almost_empty_cond0, o_almost_empty_cond1;
    reg pop0_cond, pop1_cond, push0_cond, push1_cond;

    assign almost_full_D2_cond = {o_almost_full_cond1 | o_full_cond1, o_almost_full_cond0 | o_full_cond0};
    
    SP_COM_cond SP0(.clk_8f(clk_8f),
                    .reset_L(reset_L),
                    .data_in(data_inSP0),
                    .valid_out(valid_outSP0_cond),
                    .data_out(data_out_SP0_cond));
    
    SP_COM_cond SP1(.clk_8f(clk_8f),
                    .reset_L(reset_L),
                    .data_in(data_inSP1),
                    .valid_out(valid_outSP1_cond),
                    .data_out(data_out_SP1_cond));

    // Se emplea el FIFO como se empleó en la etapa de enrutamiento 
    // arbitro2_cond arbitro2_cond (
    //                     .empty_clasif               (2'b11),
    //                     .empty_enrut                ({o_empty_cond0, o_empty_cond1}),
    //                     .pop_enrut_cond             (pop_enrut_cond[1:0])
    //                     );

    

    fifo_cond #(.BW(BW), .LEN(LEN)) FIFO_SP0 (.i_clk(clk),
                        .i_reset(reset_L),
                        .i_wr(push0_cond),
                        .i_data(data_out_SP0_cond[7:0]),
                        .i_rd(pop0_cond),
                        .o_data_cond(data_out0_cond),
                        .o_full_cond(o_full_cond0),
                        .o_empty_cond(o_empty_cond0),
                        .o_almost_full_cond(o_almost_full_cond0),
                        .o_almost_empty_cond(o_almost_empty_cond0));

    fifo_cond #(.BW(BW), .LEN(LEN)) FIFO_SP1 ( .i_clk(clk),
                        .i_reset(reset_L),
                        .i_wr(push1_cond),
                        .i_data(data_out_SP1_cond[7:0]),
                        .i_rd(pop1_cond),
                        .o_data_cond(data_out1_cond),
                        .o_full_cond(o_full_cond1),
                        .o_empty_cond(o_empty_cond1),
                        .o_almost_full_cond(o_almost_full_cond1),
                        .o_almost_empty_cond(o_almost_empty_cond1));

    always @(*)
    begin
        push0_cond =  valid_outSP0_cond;
        push1_cond =  valid_outSP1_cond;
        pop0_cond = (~o_empty_cond0) & pop[0];
        pop1_cond = (~o_empty_cond1) & pop[1];
    end

endmodule