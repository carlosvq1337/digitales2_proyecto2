
module dmux12_10b_cond #(  
                            parameter BW = 10
)

                        (   input clk,
                            input reset_L,
                            input selector,
                            output reg [BW-1:0] data_out0,
                            output reg valid_out0,
                            output reg[BW-1:0] data_out1,
                            output reg valid_out1,
                            input valid_in,
                            input [BW-1:0] data_in
                        );

                        
    reg [BW-1:0] data_recordar0;
    reg [BW-1:0] data_recordar1;
    reg valid_recordar0;
    reg valid_recordar1;

    always @(*) begin

        
        if (selector == 0 && valid_in == 1) begin
            data_recordar0 = data_in;
            data_recordar1 = 0;
            valid_recordar0 = valid_in;
            valid_recordar1 = 0;


        end else if (selector == 1 && valid_in == 1) begin
            data_recordar0 = 0;
            data_recordar1 = data_in;
            valid_recordar0 = 0;
            valid_recordar1 = valid_in;

        end else begin
            data_recordar0 = 0;
            data_recordar1 = 0;
            valid_recordar0 = 0;
            valid_recordar1 = 0;
        end
    end

    always @(posedge clk) begin
        if (reset_L == 1) begin
            if (selector == 0) begin
                data_out0 <= data_recordar0;
                valid_out0 <= valid_recordar0;
                valid_out1 <= 0; //para evitar falsos pushes
            end else if (selector == 1) begin
                data_out1 <= data_recordar1;
                valid_out1 <= valid_recordar1;
                valid_out0 <= 0; //para evitar falsos pushes
            end
        end else begin
            data_out0 <= 0;
            data_out1 <= 0;
            valid_out0 <= 0;
            valid_out1 <= 0;
        end
    end

endmodule
