`timescale 1ns / 100ps
`include "probador_fifo.v"
`include "./cells/cmos_cells.v"
`include "./FIFOs/FIFO_cond.v"
`include "./FIFOs/FIFO_estr.v"

module testbench;

//PARAMS
parameter BW=8;

//INPUTS
wire i_clk, i_reset, i_wr, i_rd;
wire [(BW-1):0] i_data;

//OUTPUTS
wire [(BW-1):0] o_data_cond, o_data_estr;
wire o_error_cond, o_error_estr, o_full_cond, o_empty_cond, o_full_estr, o_empty_estr;
wire o_almost_empty_cond, o_almost_full_cond, o_almost_empty_estr, o_almost_full_estr;

fifo_cond fifo(
    .i_clk      (i_clk),
    .i_reset    (i_reset),
    .i_wr       (i_wr),
    .i_rd       (i_rd),
    .i_data     (i_data[(BW-1):0]),
    .o_data_cond     (o_data_cond[(BW-1):0]),
    .o_error_cond    (o_error_cond),
    .o_full_cond     (o_full_cond),
    .o_empty_cond    (o_empty_cond),
    .o_almost_full_cond     (o_almost_full_cond),
    .o_almost_empty_cond    (o_almost_empty_cond)
);

fifo_estr fifo_estr(
    .i_clk      (i_clk),
    .i_reset    (i_reset),
    .i_wr       (i_wr),
    .i_rd       (i_rd),
    .i_data     (i_data[(BW-1):0]),
    .o_data_estr      (o_data_estr[(BW-1):0]),
    .o_error_estr     (o_error_estr),
    .o_full_estr      (o_full_estr),
    .o_empty_estr     (o_empty_estr),
    .o_almost_full_estr     (o_almost_full_estr),
    .o_almost_empty_estr    (o_almost_empty_estr)
);

probador p_fifo (
    .i_clk      (i_clk),
    .i_reset    (i_reset),
    .i_wr       (i_wr),
    .i_rd       (i_rd),
    .i_data     (i_data[(BW-1):0]),
    .o_data_cond     (o_data_cond[(BW-1):0]),
    .o_data_estr     (o_data_estr[(BW-1):0]),
    .o_error_cond    (o_error_cond),
    .o_error_estr    (o_error_estr),
    .o_full_cond     (o_full_cond),
    .o_empty_cond    (o_empty_cond),
    .o_full_estr     (o_full_estr),
    .o_empty_estr    (o_empty_estr),
    .o_almost_full_cond     (o_almost_full_cond),
    .o_almost_empty_cond    (o_almost_empty_cond),
    .o_almost_full_estr     (o_almost_full_estr),
    .o_almost_empty_estr    (o_almost_empty_estr)
);

endmodule