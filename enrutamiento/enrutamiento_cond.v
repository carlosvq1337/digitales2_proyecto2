//`include "./demux/dmux12_8b_cond.v"
// `include "./mux/mux21_10b_cond.v"
// `include "./FIFOs/FIFO_cond.v"

module enrutamiento_cond #(  parameter BW=8,
                            parameter LEN=6)
                        (   input clk,
                            input reset_L,
                            input selector,
                            input [9:0] data_inP0,
                            input valid_P0, // ES EL POP DEL FIFO ANTERIOR, VIENE DEL ARBITRO
                            input [9:0] data_inP1,
                            input valid_P1, // ES EL POP DEL FIFO ANTERIOR, VIENE DEL ARBITRO
                            input push, // Senal de push del arbitro
                            input [1:0] i_rd, // Senal de pop del arbitro
                            output dest_to_arb,
                            output	     [(BW-1):0]	o_data_condPS0,
	                        output   				o_full_condPS0,
	                        output   				o_empty_condPS0,
	                        output   				o_almost_full_condPS0,
	                        output   				o_almost_empty_condPS0,
                            output	     [(BW-1):0]	o_data_condPS1,
	                        output   				o_full_condPS1,
	                        output   				o_empty_condPS1,
	                        output   				o_almost_full_condPS1,
	                        output   				o_almost_empty_condPS1);

    wire [9:0] data_out_mux;
    wire dest, valid_out_mux;
    
    mux21_10b_cond mux_in(  .clk(clk),
                            .reset_L(reset_L),
                            .selector(selector),
                            .data_in0(data_inP0[9:0]),
                            .data_in1(data_inP1[9:0]),
                            .valid_0(valid_P0),
                            .valid_1(valid_P1),
                            .valid_out(valid_out_mux),
                            .data_out_cond(data_out_mux[9:0]));

    wire [7:0] data_in_dmux, dmux_out_to_FIFO_PS0, dmux_out_to_FIFO_PS1;
    assign data_in_dmux = data_out_mux[7:0];
    assign dest_to_arb = data_out_mux[8];
    wire dmux_valid_out0, dmux_valid_out1;

    dmux12_10b_cond #(.BW(BW)) dmux_dest (
                                .clk(clk),
                                .reset_L(reset_L),
                                .selector(data_out_mux[8]),
                                .valid_in(valid_out_mux),
                                .data_in(data_in_dmux[7:0]),
                                .data_out0(dmux_out_to_FIFO_PS0[7:0]),
                                .valid_out0(dmux_valid_out0),
                                .data_out1(dmux_out_to_FIFO_PS1[7:0]),
                                .valid_out1(dmux_valid_out1));

    fifo_cond #(.BW(BW), .LEN(LEN), .TOL(2)) FIFO_PS0 (.i_clk(clk),
                        .i_reset(reset_L),
                        .i_wr(dmux_valid_out0),
                        .i_data(dmux_out_to_FIFO_PS0[7:0]),
                        .i_rd(i_rd[0]),
                        .o_data_cond(o_data_condPS0),
                        .o_full_cond(o_full_condPS0),
                        .o_empty_cond(o_empty_condPS0),
                        .o_almost_full_cond(o_almost_full_condPS0),
                        .o_almost_empty_cond(o_almost_empty_condPS0));

    fifo_cond #(.BW(BW), .LEN(LEN), .TOL(2)) FIFO_PS1 ( .i_clk(clk),
                        .i_reset(reset_L),
                        .i_wr(dmux_valid_out1),
                        .i_data(dmux_out_to_FIFO_PS1[7:0]),
                        .i_rd(i_rd[1]),
                        .o_data_cond(o_data_condPS1),
                        .o_full_cond(o_full_condPS1),
                        .o_empty_cond(o_empty_condPS1),
                        .o_almost_full_cond(o_almost_full_condPS1),
                        .o_almost_empty_cond(o_almost_empty_condPS1));
endmodule