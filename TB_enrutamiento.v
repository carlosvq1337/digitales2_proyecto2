`timescale 1ns / 100ps
`include "probador_clasificacion.v"
`include "./cells/cmos_cells.v"
`include "./clasificacion/clasificacion_cond.v"
`include "./enrutamiento/enrutamiento_cond.v"

module testbench;

//PARAMS
parameter BW=10;

//INPUTS
wire clk, reset_L, clase, dest, push;
wire [7:0] data_in;
wire [1:0] i_rd;

//OUTPUTS
wire [1:0] pop_clasif_cond, pop_enrut_cond;
wire [(BW-1):0] o_data_condP0, o_data_condP1;
wire o_full_condP0, o_full_condP1, o_empty_condP0, o_empty_condP1, selector_mux_cond, push_cond;
wire o_almost_full_condP0, o_almost_full_condP1, o_almost_empty_condP0, o_almost_empty_condP1;
clasificacion_cond clasificacion_cond(
    //inputs
    .data_in                    (data_in[7:0]),
    .clk                        (clk),
    .push                       (push),
    .reset_L                    (reset_L),
    .clase                      (clase),
    .dest                       (dest),
    .i_rd                       (i_rd[1:0]),
    
    //outputs
    .o_data_condP0              (o_data_condP0[(BW-1):0]),
    .o_full_condP0              (o_full_condP0),
    .o_empty_condP0             (o_empty_condP0),
    .o_almost_full_condP0       (o_almost_empty_condP0),
    .o_almost_empty_condP0      (o_almost_empty_condP0),
    .o_data_condP1              (o_data_condP1[(BW-1):0]),
    .o_full_condP1              (o_full_condP1),
    .o_empty_condP1             (o_empty_condP1),
    .o_almost_full_condP1       (o_almost_full_condP1),
    .o_almost_empty_condP1      (o_almost_empty_condP1)

);

enrutamiento_cond enrutamiento_cond (
    .clk                        (clk),
    .reset_L                    (reset_L),
    .selector                   (selector_mux_cond), //de arbitro, por ahora del probador
    .data_inP0                  (o_data_condP0[(BW-1):0]),
    .valid_P0                   (pop_clasif_cond[0]),   //de arb
    .data_inP1                  (o_data_condP1[(BW-1):0]),
    .valid_P1                   (pop_clasif_cond[1]),      //de arb
    .push                       (push_cond), //de arb
    .i_rd                       (pop_enrut_cond[1:0]), //de arb
    .o_data_condPS0             (o_data_condPS0),
    .o_full_condPS0             (o_full_condPS0),
    .o_empty_condPS0            (o_empty_condPS0),
    .o_almost_full_condPS0      (o_almost_full_condPS0),
    .o_almost_empty_condPS0      (o_almost_empty_condPS0),
    .o_data_condPS1             (o_data_condPS1),
    .o_full_condPS1             (o_full_condPS1),
    .o_empty_condPS1            (o_empty_condPS1),
    .o_almost_full_condPS1      (o_almost_full_condPS1),
    .o_almost_empty_condPS1      (o_almost_empty_condPS1)
);



probador p_clasif (
    //inputs
    .data_in                    (data_in[7:0]),
    .clk                        (clk),
    .push                       (push),
    .reset_L                    (reset_L),
    .clase                      (clase),
    .dest                       (dest),
    .i_rd                       (i_rd[1:0]),
    
    //outputs
    .o_data_condP0              (o_data_condP0[(BW-1):0]),
    .o_full_condP0              (o_full_condP0),
    .o_empty_condP0             (o_empty_condP0),
    .o_almost_full_condP0       (o_almost_empty_condP0),
    .o_almost_empty_condP0      (o_almost_empty_condP0),
    .o_data_condP1              (o_data_condP1[(BW-1):0]),
    .o_full_condP1              (o_full_condP1),
    .o_empty_condP1             (o_empty_condP1),
    .o_almost_full_condP1       (o_almost_full_condP1),
    .o_almost_empty_condP1      (o_almost_empty_condP1)
);

endmodule