// `include "./demux/dmux12_8b_cond.v"
// `include "./FIFOs/FIFO_cond.v"

module clasificacion_cond#( parameter BW = 10,
                            parameter LEN = 10)
                           (input [7:0] data_in,
                            input clk,
                            input reset_L,
                            input clase,  // Clase, se usa como selector
                            input dest,  // Destino, definido en el probador
                            input push, // Valid_in
                            input [1:0] i_rd, // Senal de pop del arbitro
                            output	     [(BW-1):0]	o_data_condP0,
	                        output   				o_full_condP0,
	                        output   				o_empty_condP0,
	                        output   				o_almost_full_condP0,
	                        output   				o_almost_empty_condP0,
                            output	     [(BW-1):0]	o_data_condP1,
	                        output   				o_full_condP1,
	                        output   				o_empty_condP1,
	                        output   				o_almost_full_condP1,
	                        output   				o_almost_empty_condP1);
    wire [9:0] data_in10b;
    assign data_in10b = {clase, dest, data_in};

    wire [9:0] dmux_out_to_FIFO_P0, dmux_out_to_FIFO_P1;
    wire dmux_valid_out0, dmux_valid_out1, dmux_push_out0, dmux_push_out1;

    dmux12_10b_cond dmux_in( .clk(clk),
                            .reset_L(reset_L),
                            .selector(clase),
                            .valid_in(push),
                            .data_in(data_in10b),
                            .data_out0(dmux_out_to_FIFO_P0),
                            .valid_out0(dmux_valid_out0),
                            .data_out1(dmux_out_to_FIFO_P1),
                            .valid_out1(dmux_valid_out1));

    fifo_cond FIFO_P0(  .i_clk(clk),
                        .i_reset(reset_L),
                        .i_wr(dmux_valid_out0),
                        .i_data(dmux_out_to_FIFO_P0),
                        .i_rd(i_rd[0]),
                        .o_data_cond(o_data_condP0),
                        .o_full_cond(o_full_condP0),
                        .o_empty_cond(o_empty_condP0),
                        .o_almost_full_cond(o_almost_full_condP0),
                        .o_almost_empty_cond(o_almost_empty_condP0));
                
    fifo_cond FIFO_P1(  .i_clk(clk),
                        .i_reset(reset_L),
                        .i_wr(dmux_valid_out1),
                        .i_data(dmux_out_to_FIFO_P1),
                        .i_rd(i_rd[1]),
                        .o_data_cond(o_data_condP1),
                        .o_full_cond(o_full_condP1),
                        .o_empty_cond(o_empty_condP1),
                        .o_almost_full_cond(o_almost_full_condP1),
                        .o_almost_empty_cond(o_almost_empty_condP1));

endmodule