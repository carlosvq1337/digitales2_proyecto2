module memoria_cond #(parameter BW = 10,
						parameter LEN = 8)
(
	input [BW-1:0] datain,
	input [2:0] addr_in,
	input [2:0] addr_out,
	input reset, clk,read,write,
	output reg [BW-1:0] o_data_cond);

	reg [BW-1:0] mem [LEN-1:0];
	integer i;

	always@(posedge clk) begin
		if(~reset) begin
			for(i = 0; i<LEN ; i = i+1 )begin 
				mem[i] <= 'b0; //poner en 0 cada una de las posiciones de la memoria
			end
		end else begin  
			  	if (write) mem[addr_in] <= datain;
				
		end
	end

	always@(*) begin
		o_data_cond =0;
		if (read) o_data_cond= mem[addr_out];
	end


endmodule
