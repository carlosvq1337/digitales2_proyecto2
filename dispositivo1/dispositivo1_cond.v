// `include "./clasificacion/clasificacion_cond.v"
// `include "./enrutamiento/enrutamiento_cond.v"
// `include "./arbitro/arbitro_cond.v"
//`include "./serializacion/serializacion_cond.v"

module dispositivo1_cond(   input clk,
                            input clk_8f,
                            input reset_L,
                            input [7:0] data_in,
                            input clase,
                            input dest,
                            input push,
                            input [1:0] almost_full_D2,
                            output data_out_serial0_cond,
                            output data_out_serial1_cond);
    
    wire [1:0] pop_enrut_cond, pop_clasif_cond, almost_full_D2, almost_full_enrut;
    wire [7:0] o_data_condPS0, o_data_condPS1;
    wire [9:0] o_data_condP0, o_data_condP1;
    wire o_full_condP0, o_full_condP1, o_empty_condP0, o_empty_condP1, selector_mux_cond, push_cond;
    wire o_almost_full_condP0, o_almost_full_condP1, o_almost_empty_condP0, o_almost_empty_condP1;
    wire dest_to_arb, o_full_condPS0, o_full_condPS1, o_empty_condPS0, o_empty_condPS1;

    clasificacion_cond clasificacion_cond(
                        //inputs
                        .data_in                    (data_in[7:0]),
                        .clk                        (clk),
                        .push                       (push),
                        .reset_L                    (reset_L),
                        .clase                      (clase),
                        .dest                       (dest),
                        .i_rd                       (pop_clasif_cond[1:0]),
                        //outputs
                        .o_data_condP0              (o_data_condP0),
                        .o_full_condP0              (o_full_condP0),
                        .o_empty_condP0             (o_empty_condP0),
                        .o_almost_full_condP0       (o_almost_full_condP0),
                        .o_almost_empty_condP0      (o_almost_empty_condP0),
                        .o_data_condP1              (o_data_condP1),
                        .o_full_condP1              (o_full_condP1),
                        .o_empty_condP1             (o_empty_condP1),
                        .o_almost_full_condP1       (o_almost_full_condP1),
                        .o_almost_empty_condP1      (o_almost_empty_condP1)
                        );
        
    arbitro_cond arbitro_cond (
                        .empty_clasif               ({o_empty_condP1, o_empty_condP0}),
                        .empty_enrut                ({o_empty_condPS1, o_empty_condPS0}),
                        .full                       (almost_full_enrut),
                        .almost_full_D2             (almost_full_D2),
                        .dest                       (dest_to_arb),
                        .push_cond                  (push_cond),
                        .pop_clasif_cond            (pop_clasif_cond[1:0]),
                        .pop_enrut_cond             (pop_enrut_cond[1:0]),
                        .selector_mux_cond          (selector_mux_cond)
                        );

    enrutamiento_cond enrutamiento_cond (
                        .clk                        (clk),
                        .reset_L                    (reset_L),
                        .selector                   (selector_mux_cond), //de arbitro, por ahora del probador
                        .data_inP0                  (o_data_condP0),
                        .valid_P0                   (pop_clasif_cond[0]),   //de arb
                        .data_inP1                  (o_data_condP1),
                        .valid_P1                   (pop_clasif_cond[1]),      //de arb
                        .push                       (push_cond), //de arb
                        .i_rd                       (pop_enrut_cond[1:0]), //de arb
                        .dest_to_arb                (dest_to_arb),
                        .o_data_condPS0             (o_data_condPS0),
                        .o_full_condPS0             (o_full_condPS0),
                        .o_empty_condPS0            (o_empty_condPS0),
                        .o_almost_full_condPS0      (o_almost_full_condPS0),
                        .o_almost_empty_condPS0     (o_almost_empty_condPS0),
                        .o_data_condPS1             (o_data_condPS1),
                        .o_full_condPS1             (o_full_condPS1),
                        .o_empty_condPS1            (o_empty_condPS1),
                        .o_almost_full_condPS1      (o_almost_full_condPS1),
                        .o_almost_empty_condPS1     (o_almost_empty_condPS1)
                        );
    
    serializacion_cond serializacion_cond (
                        .clk_8f                     (clk_8f),
                        .reset_L                    (reset_L),
                        .data_inPS0                 (o_data_condPS0),
                        .valid_inPS0                (pop_enrut_cond[0]),
                        .data_inPS1                 (o_data_condPS1),
                        .valid_inPS1                (pop_enrut_cond[1]),
                        .data_out_serial0_cond           (data_out_serial0_cond),
                        .data_out_serial1_cond           (data_out_serial1_cond)
                        );

    assign almost_full_enrut[0] = (o_almost_full_condPS0 || o_full_condPS0);
    assign almost_full_enrut[1] = (o_almost_full_condPS1 || o_full_condPS1);

endmodule